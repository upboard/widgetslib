# Forked packages
- hot-formula-parser was forked to support HLOOKUP and VLOOKUP formulas

# Scripts
- build-dist is used to build files from src directory without webpack
- build-index builds index file from all files from src directory with webpack