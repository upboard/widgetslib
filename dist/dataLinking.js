'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var getUniqueKey = exports.getUniqueKey = function getUniqueKey(_ref) {
    var widgetUuid = _ref.widgetUuid,
        cardUuid = _ref.cardUuid;
    return cardUuid + '-' + widgetUuid;
};

var isLinksBroken = exports.isLinksBroken = function isLinksBroken(links) {
    if (!links) {
        return false;
    }

    var keys = Object.keys(links);

    return keys.some(function (key) {
        return !!links[key].errorCode;
    });
};

var resolveLink = exports.resolveLink = function resolveLink(options) {
    var chartData = options.chartData,
        dataLink = options.dataLink,
        point = options.point,
        indexesResolver = options.indexesResolver;

    var indexes = indexesResolver(point);

    return Object.values(indexes.reduce(function (acc, index) {
        var cell = chartData[index.x] && chartData[index.x][index.y];

        if (!cell || cell && !cell.dataLink) {
            return acc;
        }

        var link = dataLink && dataLink[cell.dataLink];

        if (!link) {
            return acc;
        }

        var title = link.providerInfo && link.providerInfo.title;

        if (title && !acc[getUniqueKey(link.widgetKey)]) {
            acc[getUniqueKey(link.widgetKey)] = title;
        }

        return acc;
    }, {}));
};

var getCellValue = exports.getCellValue = function getCellValue(cell) {
    if (cell && (typeof cell === 'undefined' ? 'undefined' : _typeof(cell)) === 'object' && !Array.isArray(cell)) {
        return cell.calculatedValue !== undefined ? cell.calculatedValue : cell.value;
    }
    return cell;
};

var trimData = exports.trimData = function trimData(data) {
    var lastRowIndex = 0;
    var lastColIndex = 0;

    for (var rowIndex = 0; rowIndex < data.length; rowIndex += 1) {
        var row = data[rowIndex] || [];

        for (var colIndex = 0; colIndex < row.length; colIndex += 1) {
            var cell = row[colIndex];
            var value = getCellValue(cell);

            if (value || value === 0) {
                lastRowIndex = lastRowIndex < rowIndex ? rowIndex : lastRowIndex;
                lastColIndex = lastColIndex < colIndex ? colIndex : lastColIndex;
            }
        }
    }

    var res = data.slice(0, lastRowIndex + 1);

    return res.map(function (row) {
        return row ? row.slice(0, lastColIndex + 1) : [];
    });
};