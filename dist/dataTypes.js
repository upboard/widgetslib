'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var PREFIX = 'upboard.';
var TEXT_TYPE = exports.TEXT_TYPE = PREFIX + 'text';
var DATE_TYPE = exports.DATE_TYPE = PREFIX + 'date';
var USER_TYPE = exports.USER_TYPE = PREFIX + 'user';
var HYPERLINK_TYPE = exports.HYPERLINK_TYPE = PREFIX + 'hyperLink';
var SMARTLINK_TYPE = exports.SMARTLINK_TYPE = PREFIX + 'smartLink';
var NUMERIC_TYPE = exports.NUMERIC_TYPE = PREFIX + 'numeric';

var getTextType = exports.getTextType = function getTextType(_ref) {
    var value = _ref.value,
        meta = _ref.meta;
    return {
        value: value,
        dataType: TEXT_TYPE,
        meta: meta
    };
};

var getDateType = exports.getDateType = function getDateType(_ref2) {
    var value = _ref2.value,
        meta = _ref2.meta;
    return {
        value: value,
        dataType: DATE_TYPE,
        meta: _extends({}, meta, {
            isDropdown: false
        })
    };
};

var getUserType = exports.getUserType = function getUserType(_ref3) {
    var value = _ref3.value,
        meta = _ref3.meta;
    return {
        value: value,
        dataType: USER_TYPE,
        meta: _extends({}, meta, {
            isDropdown: true,
            dropdownOptions: []
        })
    };
};

var getHyperLinkType = exports.getHyperLinkType = function getHyperLinkType(_ref4) {
    var value = _ref4.value,
        linkUrl = _ref4.linkUrl,
        meta = _ref4.meta;
    return {
        value: value,
        dataType: HYPERLINK_TYPE,
        linkUrl: linkUrl,
        meta: _extends({}, meta, {
            openingOption: ''
        })
    };
};

var getSmartLinkType = exports.getSmartLinkType = function getSmartLinkType(_ref5) {
    var value = _ref5.value,
        linkUrl = _ref5.linkUrl,
        meta = _ref5.meta;
    return {
        value: value,
        dataType: SMARTLINK_TYPE,
        linkUrl: linkUrl,
        meta: _extends({}, meta, {
            openingOption: ''
        })
    };
};

var getNumericType = exports.getNumericType = function getNumericType(_ref6) {
    var value = _ref6.value,
        meta = _ref6.meta;
    return {
        value: value,
        dataType: NUMERIC_TYPE,
        meta: meta
    };
};

var getValue = exports.getValue = function getValue(data) {
    var value = {};
    var dataType = data.dataType;

    switch (dataType) {
        case TEXT_TYPE:
            value = getTextType(data);
            break;
        case DATE_TYPE:
            value = getDateType(data);
            break;
        case USER_TYPE:
            value = getUserType(data);
            break;
        case HYPERLINK_TYPE:
            value = getHyperLinkType(data);
            break;
        case SMARTLINK_TYPE:
            value = getSmartLinkType(data);
            break;
        case NUMERIC_TYPE:
            value = getNumericType(data);
            break;
        default:
            value = getTextType(data);
    }
    return _extends({}, data, value);
};