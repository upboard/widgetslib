'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _dataTypes = require('./dataTypes');

var types = _interopRequireWildcard(_dataTypes);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var UBFToWidgetAdapter = function () {
    function UBFToWidgetAdapter(widgetType) {
        _classCallCheck(this, UBFToWidgetAdapter);

        this.typeNameFrom = 'io.upboard.common.ubf';
        this.typeNameTo = widgetType;
        this.data = [[]];
    }

    /* eslint-disable class-methods-use-this */


    _createClass(UBFToWidgetAdapter, [{
        key: 'getWidgetData',
        value: function getWidgetData(widget) {
            return widget.data;
        }
    }, {
        key: 'setData',
        value: function setData(data) {
            this.data = data;
        }
    }, {
        key: 'getData',
        value: function getData() {
            return this.data;
        }
    }, {
        key: 'getValue',
        value: function getValue(cell) {
            return cell.value;
        }
    }, {
        key: 'parseData',
        value: function parseData(data) {
            var _this = this;

            return data.map(function (row) {
                return row.map(function (_cell) {
                    var cell = _cell || {
                        dataType: types.TEXT_TYPE,
                        value: '',
                        meta: {}
                    };
                    var cellValue = types.getValue(cell);
                    if (cell.dataLink && cell.calculatedValue !== undefined) {
                        cellValue.value = cell.calculatedValue;
                    } else {
                        cellValue.calculatedValue = cell.calculatedValue;
                        cellValue.value = _this.getValue(cell);
                    }
                    return cellValue;
                });
            });
        }

        /* eslint-enable class-methods-use-this */

    }, {
        key: 'isConvertable',
        value: function isConvertable(widget) {
            var data = this.getWidgetData(widget);
            if (!Array.isArray(data) || !data.length) {
                return Promise.resolve(false);
            }
            return Promise.resolve(true);
        }
    }, {
        key: 'convert',
        value: function convert(_widget) {
            var widget = _widget;
            var data = this.getWidgetData(widget);
            this.setData(this.parseData(data));
            widget.dataFormatCode = this.typeNameTo;
            widget.rows = JSON.stringify(this.getData());
            return Promise.resolve(widget);
        }
    }]);

    return UBFToWidgetAdapter;
}();

exports.default = UBFToWidgetAdapter;