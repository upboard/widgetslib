'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }(); // eslint-disable-next-line import/no-extraneous-dependencies


var _hotFormulaParser = require('hot-formula-parser');

var _dataTypes = require('./dataTypes');

var types = _interopRequireWildcard(_dataTypes);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var WidgetToUBFAdapter = function () {
    function WidgetToUBFAdapter(widgetType) {
        _classCallCheck(this, WidgetToUBFAdapter);

        this.typeNameFrom = widgetType;
        this.typeNameTo = 'io.upboard.common.ubf';
        this.data = [[]];
        this.formulasCells = [];
    }

    /* eslint-disable class-methods-use-this */


    _createClass(WidgetToUBFAdapter, [{
        key: 'getWidgetData',
        value: function getWidgetData(widget) {
            return widget.data;
        }

        // return value data in format:
        // {
        //     value,
        //     *additionalFields
        // }

    }, {
        key: 'getValue',
        value: function getValue(cellValue) {
            return { value: cellValue.value };
        }
    }, {
        key: 'getMeta',
        value: function getMeta() {
            return {};
        }
    }, {
        key: 'getDataType',
        value: function getDataType() {
            return types.TEXT_TYPE;
        }

        // to get cell from data matrix

    }, {
        key: 'getDataCell',
        value: function getDataCell(widget, rowIndex, colIndex) {
            if (widget.data) {
                var cell = widget.data[rowIndex] && widget.data[rowIndex][colIndex];
                if (cell) {
                    return cell;
                }
            }
            return {};
        }
    }, {
        key: 'getCell',
        value: function getCell(cell, widget, rowIndex, colIndex) {
            var _parseCell2 = this._parseCell(cell),
                cellValue = _parseCell2.cellValue,
                isFormula = _parseCell2.isFormula;

            var dataCell = this.getDataCell(widget, rowIndex, colIndex);
            var dataType = cellValue.dataLink ? dataCell.dataType : this.getDataType(cellValue, widget, rowIndex, colIndex);
            var value = this.getValue(cellValue, dataType, widget, rowIndex, colIndex);
            var meta = cellValue.dataLink ? dataCell.meta : this.getMeta(cellValue, dataType, widget, rowIndex, colIndex);
            return {
                cellValue: _extends({}, cellValue, value, {
                    meta: meta,
                    dataType: dataType,
                    x: colIndex,
                    y: rowIndex
                }),
                isFormula: isFormula
            };
        }

        /* return {
            valueData: { value, dataLink },
            isFormula
        } */

    }, {
        key: '_parseCell',
        value: function _parseCell(cell) {
            var cellValue = {};
            var isFormula = false;
            if (cell && (typeof cell === 'undefined' ? 'undefined' : _typeof(cell)) === 'object' && !Array.isArray(cell)) {
                cellValue.value = cell.value;
                if (cell.dataLink) {
                    cellValue.dataLink = cell.dataLink;
                }
                if (cell.linkUrl) {
                    cellValue.linkUrl = cell.linkUrl;
                }
                isFormula = cell.calculatedValue !== undefined;
            } else {
                cellValue.value = cell;
            }
            return { cellValue: cellValue, isFormula: isFormula };
        }

        /* eslint-enable class-methods-use-this */

    }, {
        key: '_getCellValue',
        value: function _getCellValue(row, col) {
            return this.data[row][col].value;
        }
    }, {
        key: '_getCellCalculatedValue',
        value: function _getCellCalculatedValue(row, col) {
            if (this.data[row][col].calculatedValue !== undefined) {
                return this.data[row][col].calculatedValue;
            }
            return this._getCellValue(row, col);
        }
    }, {
        key: 'setData',
        value: function setData(data) {
            this.data = data;
        }
    }, {
        key: 'getData',
        value: function getData() {
            return this.data;
        }
    }, {
        key: 'setFormulasCells',
        value: function setFormulasCells(formulasCells) {
            this.formulasCells = formulasCells;
        }
    }, {
        key: '_insertCalculatedValue',
        value: function _insertCalculatedValue(row, col, calculatedValue) {
            if (!this.data[row][col]) {
                this.data[row][col] = {};
            }
            this.data[row][col].calculatedValue = calculatedValue;
        }
    }, {
        key: '_initializeParser',
        value: function _initializeParser() {
            this.parser = new _hotFormulaParser.Parser();

            var _onCallCellValue = this._onCallCellValue.bind(this);
            var _onCellRangeValue = this._onCellRangeValue.bind(this);
            this.parser.on('callCellValue', _onCallCellValue);
            this.parser.on('callRangeValue', _onCellRangeValue);
        }
    }, {
        key: '_onCallCellValue',
        value: function _onCallCellValue(cellCoord, done) {
            var cell = this._getCellCalculatedValue(cellCoord.row.index, cellCoord.column.index);
            done(cell);
        }
    }, {
        key: '_onCellRangeValue',
        value: function _onCellRangeValue(startCellCoord, endCellCoord, done) {
            var fragment = [];

            for (var row = startCellCoord.row.index; row <= endCellCoord.row.index; row += 1) {
                var colFragment = [];
                for (var col = startCellCoord.column.index; col <= endCellCoord.column.index; col += 1) {
                    colFragment.push(this._getCellValue(row, col));
                }
                fragment.push(colFragment);
            }

            if (fragment) {
                done(fragment);
            }
        }
    }, {
        key: '_parseFormula',
        value: function _parseFormula(formula) {
            if (!this.parser) {
                this._initializeParser();
            }
            var parsedFormula = this.parser.parse(formula);
            return parsedFormula.error ? parsedFormula.error : parsedFormula.result;
        }
    }, {
        key: 'calculateFormulas',
        value: function calculateFormulas() {
            var _this = this;

            this.formulasCells.forEach(function (cell) {
                var value = _this._getCellValue(cell.row, cell.col);

                if (value && typeof value === 'string') {
                    var formula = value.substr(1);
                    var calculatedValue = _this._parseFormula(formula);
                    _this._insertCalculatedValue(cell.row, cell.col, calculatedValue);
                }
            });

            this.setFormulasCells([]);
        }
    }, {
        key: 'parseData',
        value: function parseData(rows, widget) {
            var _this2 = this;

            var cellsWithFormula = [];
            var data = [[]];
            if (rows && Array.isArray(rows)) {
                rows.forEach(function (row, rowIndex) {
                    if (!data[rowIndex]) {
                        data[rowIndex] = [];
                    }
                    if (row && Array.isArray(row)) {
                        row.forEach(function (cell, colIndex) {
                            var _getCell = _this2.getCell(cell, widget, rowIndex, colIndex),
                                cellValue = _getCell.cellValue,
                                isFormula = _getCell.isFormula;

                            data[rowIndex][colIndex] = types.getValue(cellValue);
                            if (isFormula) {
                                cellsWithFormula.push({ row: rowIndex, col: colIndex });
                            }
                        });
                    }
                });
            }
            return {
                data: data,
                cellsWithFormula: cellsWithFormula
            };
        }
    }, {
        key: 'isConvertable',
        value: function isConvertable(widget) {
            var data = this.getWidgetData(widget);
            if (!Array.isArray(data) || !data.length) {
                return Promise.resolve(false);
            }
            return Promise.resolve(true);
        }
    }, {
        key: 'convert',
        value: function convert(_widget) {
            var widget = _widget;
            var widgetData = this.getWidgetData(widget);

            var _parseData = this.parseData(widgetData, widget),
                data = _parseData.data,
                cellsWithFormula = _parseData.cellsWithFormula;

            this.setData(data);
            if (cellsWithFormula) {
                this.setFormulasCells(cellsWithFormula);
                this.calculateFormulas();
            }
            widget.dataFormatCode = 'io.upboard.common.ubf';
            widget.data = this.getData();
            return Promise.resolve(widget);
        }
    }]);

    return WidgetToUBFAdapter;
}();

exports.default = WidgetToUBFAdapter;