"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

// https://leverxeu.atlassian.net/browse/EUPBOARD01-3552
// to fix problem with reflow during window resizing
var pathHighchartsReflow = exports.pathHighchartsReflow = function pathHighchartsReflow(Highcharts) {
    var _reflow = Highcharts.Chart.prototype.reflow;

    // eslint-disable-next-line no-param-reassign
    Highcharts.Chart.prototype.reflow = function (e) {
        if (this.options) {
            _reflow.call(this, e);
        }
    };
};

// to fix https://leverxeu.atlassian.net/browse/EUPBOARD01-2210
var pathHighchartsMouseDownListener = exports.pathHighchartsMouseDownListener = function pathHighchartsMouseDownListener(Highcharts) {
    // eslint-disable-next-line no-param-reassign
    Highcharts.Pointer.prototype.onContainerMouseDown = function onContainerMouseDown(e) {
        e = this.normalize(e); // eslint-disable-line no-param-reassign
        this.zoomOption(e);
        this.dragStart(e);
    };
};

/**
 * Make invisible items in legend where all values empty
 * @param {Object[]} data - chart series array
 * @param {function} isValueEmpty - detect is value empty or not
 */
var hideLabelsForEmptyRows = exports.hideLabelsForEmptyRows = function hideLabelsForEmptyRows(data, isValueEmpty) {
    return data.map(function (row) {
        var isEmpty = !row.data.some(function (point) {
            return !isValueEmpty(point);
        });

        if (isEmpty) {
            return _extends({}, row, {
                showInLegend: false,
                visible: false
            });
        }

        return row;
    });
};