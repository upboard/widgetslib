'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
// JS has problems when dealing with operation with decimals.
// For example 0.55 * 100 === 55.00000000000001.
// This function helps to avoid such weird things
var multiplyBy100 = exports.multiplyBy100 = function multiplyBy100(n) {
    var floor = Math.floor(n);
    var fraction = n - floor;
    if (fraction) {
        // get rid of small threshold by toFixed(10). Than replace all 0 with nothing.
        // And finally get rid of dot (if it is on the end).
        return (n * 100).toFixed(10).replace(/0+$/, '').replace(/\.$/, '');
    }

    return n * 100;
};

var countDecimal = function countDecimal(value) {
    if (Math.floor(value) === value) {
        return 0;
    }

    return value.toString().split('.')[1].length || 0;
};

var formatPercentage = exports.formatPercentage = function formatPercentage(value) {
    var parsedNumber = Number(value);

    if (Number.isNaN(parsedNumber)) {
        return value;
    }

    return multiplyBy100(value) + '%';
};

var formatCurrency = exports.formatCurrency = function formatCurrency(value, cell) {
    var currenciesSignMap = {
        'currency-us': '$',
        'currency-euro': '€'
    };

    var parsedNumber = Number(value);

    if (Number.isNaN(parsedNumber)) {
        return value;
    }

    var fixedNumber = countDecimal(parsedNumber) > 2 ? parsedNumber.toFixed(2) : parsedNumber;

    var formatAs = cell.meta && cell.meta.formatAs;
    var currencySign = currenciesSignMap[formatAs] || '';

    return '' + currencySign + fixedNumber;
};