import * as types from './dataTypes';

export default class UBFToWidgetAdapter {
    constructor(widgetType) {
        this.typeNameFrom = 'io.upboard.common.ubf';
        this.typeNameTo = widgetType;
        this.data = [[]];
    }

    /* eslint-disable class-methods-use-this */
    getWidgetData(widget) {
        return widget.data;
    }

    setData(data) {
        this.data = data;
    }

    getData() {
        return this.data;
    }

    getValue(cell) {
        return cell.value;
    }

    parseData(data) {
        return data.map(row => row.map((_cell) => {
            const cell = _cell || {
                dataType: types.TEXT_TYPE,
                value: '',
                meta: {}
            };
            const cellValue = types.getValue(cell);
            if (cell.dataLink && cell.calculatedValue !== undefined) {
                cellValue.value = cell.calculatedValue;
            } else {
                cellValue.calculatedValue = cell.calculatedValue;
                cellValue.value = this.getValue(cell);
            }
            return cellValue;
        }));
    }

    /* eslint-enable class-methods-use-this */

    isConvertable(widget) {
        const data = this.getWidgetData(widget);
        if (!Array.isArray(data) || !data.length) {
            return Promise.resolve(false);
        }
        return Promise.resolve(true);
    }

    convert(_widget) {
        const widget = _widget;
        const data = this.getWidgetData(widget);
        this.setData(this.parseData(data));
        widget.dataFormatCode = this.typeNameTo;
        widget.rows = JSON.stringify(this.getData());
        return Promise.resolve(widget);
    }
}
