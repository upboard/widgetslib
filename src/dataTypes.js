const PREFIX = 'upboard.';
export const TEXT_TYPE = `${PREFIX}text`;
export const DATE_TYPE = `${PREFIX}date`;
export const USER_TYPE = `${PREFIX}user`;
export const HYPERLINK_TYPE = `${PREFIX}hyperLink`;
export const SMARTLINK_TYPE = `${PREFIX}smartLink`;
export const NUMERIC_TYPE = `${PREFIX}numeric`;

export const getTextType = ({ value, meta }) => ({
    value,
    dataType: TEXT_TYPE,
    meta
});

export const getDateType = ({ value, meta }) => ({
    value,
    dataType: DATE_TYPE,
    meta: {
        ...meta,
        isDropdown: false
    }
});

export const getUserType = ({ value, meta }) => ({
    value,
    dataType: USER_TYPE,
    meta: {
        ...meta,
        isDropdown: true,
        dropdownOptions: []
    }
});

export const getHyperLinkType = ({ value, linkUrl, meta }) => ({
    value,
    dataType: HYPERLINK_TYPE,
    linkUrl,
    meta: {
        ...meta,
        openingOption: ''
    }
});

export const getSmartLinkType = ({ value, linkUrl, meta }) => ({
    value,
    dataType: SMARTLINK_TYPE,
    linkUrl,
    meta: {
        ...meta,
        openingOption: ''
    }
});

export const getNumericType = ({ value, meta }) => ({
    value,
    dataType: NUMERIC_TYPE,
    meta
});

export const getValue = (data) => {
    let value = {};
    const { dataType } = data;
    switch (dataType) {
        case TEXT_TYPE:
            value = getTextType(data);
            break;
        case DATE_TYPE:
            value = getDateType(data);
            break;
        case USER_TYPE:
            value = getUserType(data);
            break;
        case HYPERLINK_TYPE:
            value = getHyperLinkType(data);
            break;
        case SMARTLINK_TYPE:
            value = getSmartLinkType(data);
            break;
        case NUMERIC_TYPE:
            value = getNumericType(data);
            break;
        default:
            value = getTextType(data);
    }
    return { ...data, ...value };
};
