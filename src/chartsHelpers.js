// https://leverxeu.atlassian.net/browse/EUPBOARD01-3552
// to fix problem with reflow during window resizing
export const pathHighchartsReflow = (Highcharts) => {
    const _reflow = Highcharts.Chart.prototype.reflow;

    // eslint-disable-next-line no-param-reassign
    Highcharts.Chart.prototype.reflow = function (e) {
        if (this.options) {
            _reflow.call(this, e);
        }
    };
};

// to fix https://leverxeu.atlassian.net/browse/EUPBOARD01-2210
export const pathHighchartsMouseDownListener = (Highcharts) => {
    // eslint-disable-next-line no-param-reassign
    Highcharts.Pointer.prototype.onContainerMouseDown = function onContainerMouseDown(e) {
        e = this.normalize(e); // eslint-disable-line no-param-reassign
        this.zoomOption(e);
        this.dragStart(e);
    };
};

/**
 * Make invisible items in legend where all values empty
 * @param {Object[]} data - chart series array
 * @param {function} isValueEmpty - detect is value empty or not
 */
export const hideLabelsForEmptyRows = (data, isValueEmpty) => (
    data.map((row) => {
        const isEmpty = !row.data.some(point => !isValueEmpty(point));

        if (isEmpty) {
            return {
                ...row,
                showInLegend: false,
                visible: false
            };
        }

        return row;
    })
);
