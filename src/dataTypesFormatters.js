// JS has problems when dealing with operation with decimals.
// For example 0.55 * 100 === 55.00000000000001.
// This function helps to avoid such weird things
export const multiplyBy100 = (n) => {
    const floor = Math.floor(n);
    const fraction = n - floor;
    if (fraction) {
        // get rid of small threshold by toFixed(10). Than replace all 0 with nothing.
        // And finally get rid of dot (if it is on the end).
        return (n * 100).toFixed(10).replace(/0+$/, '').replace(/\.$/, '');
    }

    return n * 100;
};

const countDecimal = (value) => {
    if (Math.floor(value) === value) {
        return 0;
    }

    return value.toString().split('.')[1].length || 0;
};

export const formatPercentage = (value) => {
    const parsedNumber = Number(value);

    if (Number.isNaN(parsedNumber)) {
        return value;
    }

    return `${multiplyBy100(value)}%`;
};

export const formatCurrency = (value, cell) => {
    const currenciesSignMap = {
        'currency-us': '$',
        'currency-euro': '€'
    };

    const parsedNumber = Number(value);

    if (Number.isNaN(parsedNumber)) {
        return value;
    }

    const fixedNumber = countDecimal(parsedNumber) > 2 ? parsedNumber.toFixed(2) : parsedNumber;

    const formatAs = cell.meta && cell.meta.formatAs;
    const currencySign = currenciesSignMap[formatAs] || '';

    return `${currencySign}${fixedNumber}`;
};
