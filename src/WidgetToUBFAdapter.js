// eslint-disable-next-line import/no-extraneous-dependencies
import { Parser } from 'hot-formula-parser';

import * as types from './dataTypes';

export default class WidgetToUBFAdapter {
    constructor(widgetType) {
        this.typeNameFrom = widgetType;
        this.typeNameTo = 'io.upboard.common.ubf';
        this.data = [[]];
        this.formulasCells = [];
    }

    /* eslint-disable class-methods-use-this */
    getWidgetData(widget) {
        return widget.data;
    }

    // return value data in format:
    // {
    //     value,
    //     *additionalFields
    // }
    getValue(cellValue) {
        return { value: cellValue.value };
    }

    getMeta() {
        return {};
    }

    getDataType() {
        return types.TEXT_TYPE;
    }

    // to get cell from data matrix
    getDataCell(widget, rowIndex, colIndex) {
        if (widget.data) {
            const cell = widget.data[rowIndex] && widget.data[rowIndex][colIndex];
            if (cell) {
                return cell;
            }
        }
        return {};
    }

    getCell(cell, widget, rowIndex, colIndex) {
        const { cellValue, isFormula } = this._parseCell(cell);
        const dataCell = this.getDataCell(widget, rowIndex, colIndex);
        const dataType = cellValue.dataLink
            ? dataCell.dataType : this.getDataType(cellValue, widget, rowIndex, colIndex);
        const value = this.getValue(cellValue, dataType, widget, rowIndex, colIndex);
        const meta = cellValue.dataLink
            ? dataCell.meta : this.getMeta(cellValue, dataType, widget, rowIndex, colIndex);
        return {
            cellValue: {
                ...cellValue,
                ...value,
                meta,
                dataType,
                x: colIndex,
                y: rowIndex
            },
            isFormula
        };
    }

    /* return {
        valueData: { value, dataLink },
        isFormula
    } */
    _parseCell(cell) {
        const cellValue = {};
        let isFormula = false;
        if (cell && typeof cell === 'object' && !Array.isArray(cell)) {
            cellValue.value = cell.value;
            if (cell.dataLink) {
                cellValue.dataLink = cell.dataLink;
            }
            if (cell.linkUrl) {
                cellValue.linkUrl = cell.linkUrl;
            }
            isFormula = cell.calculatedValue !== undefined;
        } else {
            cellValue.value = cell;
        }
        return { cellValue, isFormula };
    }

    /* eslint-enable class-methods-use-this */

    _getCellValue(row, col) {
        return this.data[row][col].value;
    }

    _getCellCalculatedValue(row, col) {
        if (this.data[row][col].calculatedValue !== undefined) {
            return this.data[row][col].calculatedValue;
        }
        return this._getCellValue(row, col);
    }

    setData(data) {
        this.data = data;
    }

    getData() {
        return this.data;
    }

    setFormulasCells(formulasCells) {
        this.formulasCells = formulasCells;
    }

    _insertCalculatedValue(row, col, calculatedValue) {
        if (!this.data[row][col]) {
            this.data[row][col] = {};
        }
        this.data[row][col].calculatedValue = calculatedValue;
    }

    _initializeParser() {
        this.parser = new Parser();

        const _onCallCellValue = this._onCallCellValue.bind(this);
        const _onCellRangeValue = this._onCellRangeValue.bind(this);
        this.parser.on('callCellValue', _onCallCellValue);
        this.parser.on('callRangeValue', _onCellRangeValue);
    }

    _onCallCellValue(cellCoord, done) {
        const cell = this._getCellCalculatedValue(cellCoord.row.index, cellCoord.column.index);
        done(cell);
    }

    _onCellRangeValue(startCellCoord, endCellCoord, done) {
        const fragment = [];

        for (let row = startCellCoord.row.index; row <= endCellCoord.row.index; row += 1) {
            const colFragment = [];
            for (let col = startCellCoord.column.index;
                col <= endCellCoord.column.index; col += 1) {
                colFragment.push(this._getCellValue(row, col));
            }
            fragment.push(colFragment);
        }

        if (fragment) {
            done(fragment);
        }
    }

    _parseFormula(formula) {
        if (!this.parser) {
            this._initializeParser();
        }
        const parsedFormula = this.parser.parse(formula);
        return parsedFormula.error ? parsedFormula.error : parsedFormula.result;
    }

    calculateFormulas() {
        this.formulasCells.forEach((cell) => {
            const value = this._getCellValue(cell.row, cell.col);

            if (value && typeof value === 'string') {
                const formula = value.substr(1);
                const calculatedValue = this._parseFormula(formula);
                this._insertCalculatedValue(cell.row, cell.col, calculatedValue);
            }
        });

        this.setFormulasCells([]);
    }

    parseData(rows, widget) {
        const cellsWithFormula = [];
        const data = [[]];
        if (rows && Array.isArray(rows)) {
            rows.forEach((row, rowIndex) => {
                if (!data[rowIndex]) {
                    data[rowIndex] = [];
                }
                if (row && Array.isArray(row)) {
                    row.forEach((cell, colIndex) => {
                        const {
                            cellValue,
                            isFormula
                        } = this.getCell(cell, widget, rowIndex, colIndex);
                        data[rowIndex][colIndex] = types.getValue(cellValue);
                        if (isFormula) {
                            cellsWithFormula.push({ row: rowIndex, col: colIndex });
                        }
                    });
                }
            });
        }
        return {
            data,
            cellsWithFormula
        };
    }

    isConvertable(widget) {
        const data = this.getWidgetData(widget);
        if (!Array.isArray(data) || !data.length) {
            return Promise.resolve(false);
        }
        return Promise.resolve(true);
    }

    convert(_widget) {
        const widget = _widget;
        const widgetData = this.getWidgetData(widget);
        const { data, cellsWithFormula } = this.parseData(widgetData, widget);
        this.setData(data);
        if (cellsWithFormula) {
            this.setFormulasCells(cellsWithFormula);
            this.calculateFormulas();
        }
        widget.dataFormatCode = 'io.upboard.common.ubf';
        widget.data = this.getData();
        return Promise.resolve(widget);
    }
}
