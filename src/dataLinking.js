export const getUniqueKey = ({ widgetUuid, cardUuid }) => `${cardUuid}-${widgetUuid}`;

export const isLinksBroken = (links) => {
    if (!links) {
        return false;
    }

    const keys = Object.keys(links);

    return keys.some(key => !!links[key].errorCode);
};

export const resolveLink = (options) => {
    const {
        chartData,
        dataLink,
        point,
        indexesResolver
    } = options;
    const indexes = indexesResolver(point);

    return Object.values(indexes.reduce((acc, index) => {
        const cell = chartData[index.x] && chartData[index.x][index.y];

        if (!cell || (cell && !cell.dataLink)) {
            return acc;
        }

        const link = dataLink && dataLink[cell.dataLink];

        if (!link) {
            return acc;
        }

        const title = link.providerInfo && link.providerInfo.title;

        if (title && !acc[getUniqueKey(link.widgetKey)]) {
            acc[getUniqueKey(link.widgetKey)] = title;
        }

        return acc;
    }, {}));
};

export const getCellValue = (cell) => {
    if (cell && typeof cell === 'object' && !Array.isArray(cell)) {
        return cell.calculatedValue !== undefined ? cell.calculatedValue : cell.value;
    }
    return cell;
};

export const trimData = (data) => {
    let lastRowIndex = 0;
    let lastColIndex = 0;

    for (let rowIndex = 0; rowIndex < data.length; rowIndex += 1) {
        const row = data[rowIndex] || [];

        for (let colIndex = 0; colIndex < row.length; colIndex += 1) {
            const cell = row[colIndex];
            const value = getCellValue(cell);

            if (value || value === 0) {
                lastRowIndex = lastRowIndex < rowIndex ? rowIndex : lastRowIndex;
                lastColIndex = lastColIndex < colIndex ? colIndex : lastColIndex;
            }
        }
    }

    const res = data.slice(0, lastRowIndex + 1);

    return res.map(row => (
        row ? row.slice(0, lastColIndex + 1) : []
    ));
};
